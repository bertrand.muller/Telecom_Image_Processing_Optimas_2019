\documentclass{tnreport1}
%\documentclass[stage2a]{tnreport1} % If you are in 2nd year
%\documentclass[confidential]{tnreport1} % If you are writing confidential report

\def\reportTitle{\textbf{Partie 1} - Caractérisation d'images et traitements au niveau du pixel\\~\\
\textbf{Partie 2} - Traitements aux environs du pixel : Filtrage - Détection de contours\\~\\
\textbf{Partie 3} - Filtrage Morphologique} % Titre du mémoire
\def\reportAuthor{Ophélien Amsler \& Bertrand Müller}

\usepackage{lipsum}
\usepackage{subcaption}
\usepackage{adjustbox}
\usepackage{dirtree}
\usepackage{tikz}
\usepackage{multirow}
\usetikzlibrary{trees}
\usepackage{tikz-qtree}
\usepackage{makecell}
\usepackage{hhline}
\usepackage{colortbl}
\usepackage[most]{tcolorbox}
\usepackage{pdfpages}
\usepackage{draftwatermark}
\usepackage[stable]{footmisc}
\usepackage{hhline}
\usepackage{float}
\usepackage{graphicx}

\begin{document}

\maketitle

\clearpage

\renewcommand{\baselinestretch}{0.5}\normalsize
\tableofcontents
\renewcommand{\baselinestretch}{1.0}\normalsize

\clearpage

\chapter{Caractérisation d'images et traitements au niveau du pixel}

\section{Caractérisation d'une image}

\begin{figure}[h]
	\centering
	\fboxsep=1.2pt
	\includegraphics[scale=0.9]{../tps/TP1/1_1_histogramme}
	\caption{Histogramme de l'image \textbf{Connect\_OK}}
	\label{fig:ConnectOK_histogram}
\end{figure}

Grâce à cet histogramme, on peut constater que la dynamique de l'image est faible. On distingue alors deux zones : une zone sombre pour des valeurs de gris comprises entre \textbf{0} et \textbf{127} correspondant au fond de l'image, et une zone plus claire entre \textbf{191} et \textbf{255} qui correspond aux broches des connecteurs. 

L'histogramme du profil de ligne montre plusieurs pics. Les deux groupes de pics correspondent aux broches des deux connecteurs. Les pics se situent aux alentours de \textbf{200} contre un fond aux alentours de \textbf{60}. La luminance est donc de \textbf{140}. Les pics étant abrupts, on peut en déduire que le contraste est très important.

\clearpage

\section{Traitements d'amélioration par LUT (\textit{Look-Up Table})}

A ce stade, on souhaite utiliser la fonction de modification des LUT de sortie par action sur la bissectrice de départ. 

\textbf{1.}  Afin de se rapprocher de l'image \textbf{Connect\_OK}, il faut appliquer la LUT ci-dessous pour l'image \textbf{Connect\_L}, correspondant à la transformation linéaire identité : 

\begin{figure}[H]
	\centering
	\fboxsep=1.2pt
	\includegraphics[scale=0.4]{../tps/TP1/2_1_lut_L}
	\caption{Comparaison de l'image \textbf{Connect\_OK} et \textbf{Connect\_L}\\après application de la LUT}
	\label{fig:LUT_ConnectL}
\end{figure}

De la même manière, on peut appliquer une LUT à l'image \textbf{Connect\_D} pour se rapprocher au maximum de l'image de référence \textbf{Connect\_OK}. Cependant, pour cette fois, il a été décidé de créer la LUT manuellement. Elle peut être visualisée sur la figure \ref{fig:LUT_ConnectD}.

\begin{figure}[H]
	\centering
	\fboxsep=1.2pt
	\includegraphics[scale=0.4]{../tps/TP1/2_1_lut_D}
	\caption{Comparaison de l'image \textbf{Connect\_OK} et \textbf{Connect\_D}\\après application de la LUT}
	\label{fig:LUT_ConnectD}
\end{figure}

Globalement, l'application des LUT pour chacune des deux images est plutôt probant. En effet, les différences entre l'image de référence et les images obtenues sont visuellement minimes. De plus, ceci est confirmé par comparaison des histogrammes obtenus et celui de l'image \textbf{Connect\_OK}. En effet, ils sont très proches et présentent les mêmes caractéristiques (Figure \ref{fig:LUT_histograms}). 

\bigskip

\begin{figure}[H]
	\centering
	\fboxsep=1.2pt
	\begin{subfigure}{.3\textwidth}
		\includegraphics[scale=0.52]{../tps/TP1/1_1_histogramme}
		\caption{\textbf{Connect\_OK}}
	\end{subfigure}
	\begin{subfigure}{.3\textwidth}
		\includegraphics[scale=0.6]{../tps/TP1/2_1_result_L}
		\caption{\textbf{Connect\_L} avec LUT}
	\end{subfigure}
	\begin{subfigure}{.3\textwidth}
		\includegraphics[scale=0.6]{../tps/TP1/2_1_result_D}
		\caption{\textbf{Connect\_D} avec LUT}
	\end{subfigure}
	\caption{Comparaison des histogrammes}
	\label{fig:LUT_histograms}
\end{figure}

\textbf{2.} On souhaite faire apparaître au mieux les pattes des connecteurs sur l'image \textbf{Connect\_OK}. Pour cela, on a utilisé la fonction \textit{Threshold} du logiciel. Afin de les faire ressortir au maximum, on a choisi un seuillage compris entre \textbf{127} et \textbf{255} afin d'ignorer les niveaux de gris propres au fond foncé. Après seuillage et binarisation de l'image, on obtient le résultat suivant : 

\begin{figure}[H]
	\centering
	\fboxsep=1.2pt
	\includegraphics[scale=0.4]{../tps/TP1/2_2_seuillage_OK}
	\caption{Pattes des connecteurs de l'image \textbf{Connect\_OK}\\après seuillage et binarisation}
	\label{fig:Connecteurs_Seuillage}
\end{figure}

\textbf{3.} Il est également possible de retrouver les paramètres de seuillage par visualisation de l'histogramme sur la figure \ref{fig:ConnectOK_histogram}. En effet, étant donné le grand nombre de pixels propres au fond sombre, on peut en déduire les pixels propres aux connecteurs et plus précisément aux broches. De par leur couleur claire, les niveaux de gris correspondants se trouvent sur la partie droite de l'histogramme. Il est alors possible de déterminer un intervalle de niveaux de gris pour les broches. Cet intervalle correspondra alors aux paramètres de seuillage. 

\begin{figure}[H]
	\centering
	\begin{tabular}{rcc}
		& \textbf{Classification Error} & \textbf{Exponential Fit} \\
		\rotatebox{90}{\textbf{Connect\_OK}} & \includegraphics[scale=0.3]{../tps/TP1/2_3_classErr_ok} & \includegraphics[scale=0.3]{../tps/TP1/2_3_expFit_ok}\\
		\rotatebox{90}{\textbf{Divers1}} & \includegraphics[scale=0.3]{../tps/TP1/2_3_classErr_divers} & \includegraphics[scale=0.3]{../tps/TP1/2_3_expFit_divers}
	\end{tabular}
	\fboxsep=1.2pt
	\caption{Méthodes de seuillage \textbf{Classification Error} et \textbf{Exponential Fit}}
	\label{fig:ClassError_ExpoFit}
\end{figure}

Lors de la configuration manuelle du seuillage, la borne inférieure était de \textbf{127}. Pour le mode \textbf{Exponential Fit}, la valeur de seuil choisie automatiquement par le logiciel est de \textbf{155}. On peut considérer que cela est proche de ce que nous avions sélectionné en terme de niveaux de gris. En revanche, pour le mode \textbf{Classification Error}, le résultat est moins intéressant pour l'isolation des objets et des broches. Le seuil choisi par cette méthode est plus basse que la précédente, ce qui entraîne une confusion entre les pixels appartenant au fond et ceux appartenant aux objets.

\chapter{Traitements aux environs du pixel : Filtrage - Détection de contours} 

\section{Passe-bas}

\textbf{1.} Ici, on souhaite réaliser un filtrage de type passe-bas, c'est-à-dire qui laisse passer les basses fréquences et qui atténue les hautes fréquences. Pour illustrer l'utilisation de ces outils, on va les appliquer sur l'image de la figure \ref{fig:Filters_Origin}.

\begin{figure}[H]
	\centering
	\fboxsep=1.2pt
	\includegraphics[scale=0.3]{../resources/TP1/MEDIAN}
	\caption{Image originale}
	\label{fig:Filters_Origin}
\end{figure}

Les filtres appliqués sont les suivants : \textbf{Gaussian 3x3}, \textbf{Gaussian 5x5} et \textbf{Median}. 

\begin{figure}[H]
	\centering
	\fboxsep=1.2pt
	\begin{subfigure}{.49\textwidth}
		\includegraphics[scale=0.35]{../tps/TP1/P2_1_1_gaussia3x3_median}
		\caption{Gaussien 3x3}
	\end{subfigure}
	\begin{subfigure}{.49\textwidth}
		\includegraphics[scale=0.35]{../tps/TP1/P2_1_1_gaussian5x5_median}
		\caption{Gaussien 5x5}
	\end{subfigure}
	\begin{subfigure}{.49\textwidth}
		\includegraphics[scale=0.345]{../tps/TP1/P2_1_1_median3x3_median}
		\caption{Median 3x3}
	\end{subfigure}
	\begin{subfigure}{.49\textwidth}
		\includegraphics[scale=0.39]{../tps/TP1/P2_1_1_median5x5_median}
		\caption{Median 5x5}
	\end{subfigure}
	\caption{Images obtenues après application des filtres}	
	\label{fig:Filters_All}
\end{figure}

En ce qui concerne le filtre gaussien 3x3, les pics de luminance sont plus faibles (les plus hautes fréquences sont bien atténuées). Il est en de même pour le filtre gaussien 5x5. Toutefois, ces deux filtres rendent l'image plus floue. Ils correspondent bien à notre volonté de supprimer le bruit mais rendent les images moins exploitables.

\textbf{2.} Ici, on souhaite utiliser la transformée de Fourier  pour lisser l'image. Le résultat correspond à la figure \ref{fig:FFT}. On peut remarquer que l'image est bien lissée mais que le bruit est toujours présent (bruit de type "Salt \& Pepper"). 

\begin{figure}[H]
	\centering
	\fboxsep=1.2pt
	\includegraphics[scale=0.4]{../tps/TP1/P2_1_2_fft}
	\caption{Image obtenue après application de la transformée de Fourier}
	\label{fig:FFT}
\end{figure}

\clearpage

\section{Rehaussement de contours}

\textbf{1.} Dans cette partie, on veut rehausser les contours de l'image de la figure \ref{fig:Rehauss_Mire}. 

\begin{figure}[H]
	\centering
	\fboxsep=1.2pt
	\includegraphics[scale=0.3]{../resources/TP1/mire_gris}
	\caption{Image originale - Mire Gris}
	\label{fig:Rehauss_Mire}
\end{figure}

Pour faire apparaître des contours, il est tout d'abord nécessaire d'égaliser l'histogramme de l'image d'origine. Cet histogramme est présenté dans la figure \ref{fig:Rehauss_Egalisation}.

\begin{figure}[H]
	\centering
	\fboxsep=1.2pt
	\includegraphics[scale=0.3]{../tps/TP1/P2_2_1_egalisation}
	\caption{Egalisation d'histogramme}
	\label{fig:Rehauss_Egalisation}
\end{figure}

On peut désormais appliquer les filtres \textbf{SharpenLow}, \textbf{SharpenMed} et \textbf{SharpenHigh}. Il est possible de voir apparaître des lignes de contraste avec le phénomène de Mach, faculté de l'œil à intensifier le contraste d'une transition. Ce type de filtre peut servir à faire apparaître des contours ou à délimiter des objets et des zones.

\begin{figure}[H]
	\centering
	\fboxsep=1.2pt
	\begin{subfigure}{1.\textwidth}
		\centering
		\includegraphics[scale=0.3]{../tps/TP1/P2_2_1_luminance_sharpenlow}
		\caption{Sharpen Low}
	\end{subfigure}
	\begin{subfigure}{1.\textwidth}
		\centering
		\includegraphics[scale=0.3]{../tps/TP1/P2_2_1_luminance_sharpenmed}
		\caption{Sharpen Med}
	\end{subfigure}
	\begin{subfigure}{1.\textwidth}
		\centering
		\includegraphics[scale=0.3]{../tps/TP1/P2_2_1_luminance_sharpenhigh}
		\caption{Sharpen High}
	\end{subfigure}
	\caption{Filtres faisant apparaître le phénomène de Mach}
	\label{fig:Filters_Mach}
\end{figure}

Les étroites lignes noires et blanches, qui apparaissent à la limite de deux surfaces de clarté différente, correspondent ainsi aux bandes de Mach. La différence entre les deux teintes est ainsi augmentée. Cependant, si l'on observait ces bandes sur un fond neutre, on se rendrait compte qu'elles n'existent pas. 

\section{Détection de contours}

\textbf{1.} Sur la figure \ref{fig:Profils_Lignes}, on peut relever les niveaux de gris associés à deux profils de lignes. Il est possible d'identifier les transitions entre le fond et les objets en regardant l'histogramme de luminance. En effet, les transitions sont marquées par les changements d'amplitude brutaux.

\begin{figure}[H]
	\centering
	\fboxsep=1.2pt
	\begin{subfigure}{1.\textwidth}
		\centering
		\includegraphics[scale=0.6]{../tps/TP1/P2_3_1_luminance_1}
		\caption{Profil de ligne n°1}
	\end{subfigure}
	\begin{subfigure}{1.\textwidth}
		\centering
		\includegraphics[scale=0.6]{../tps/TP1/P2_3_1_luminance_2}
		\caption{Profil de ligne n°2}
	\end{subfigure}
	\caption{Profils de ligne}
	\label{fig:Profils_Lignes}
\end{figure}

\textbf{2.} L'idée est désormais d'appliquer l'opérateur de Sobel sur l'image et de comparer les profils de lignes après détection des contours. On peut remarquer que les contrastes sont beaucoup plus marqués (amplitudes d'histogramme encore plus fortes).

\begin{figure}[H]
	\centering
	\fboxsep=1.2pt
	\begin{subfigure}{.4\textwidth}
		\centering
		\includegraphics[scale=0.45]{../tps/TP1/P2_3_1_sobel_1}
		\caption{Profil de ligne n°1}
	\end{subfigure}
	\begin{subfigure}{.4\textwidth}
		\centering
		\includegraphics[scale=0.45]{../tps/TP1/P2_3_1_sobel_2}
		\caption{Profil de ligne n°2}
	\end{subfigure}
	\caption{Profils de ligne - Sobel}
	\label{fig:Profils_Lignes_Sobel}
\end{figure}

\chapter{Filtrage morphologique}

\section{Morphologie mathématique binaire}

\begin{figure}[H]
	\centering
	\fboxsep=1.2pt
	\includegraphics[scale=0.2]{../tps/TP1/P3_1_1_binarize}
	\caption{Image Divers\_BIN (Image Divers1 binarisée)}
	\label{fig:Divers_BIN}
\end{figure}

\textbf{1.} Après application des traitements d'érosion et de dilatation, on peut constater que l'opération d'érosion permet de supprimer les bords des objets et que la dilatation permet d'augmenter la taille des objets en ajoutant des contours successifs. Après des opérations d'érosion et de dilatation successives, on peut remarquer que les objets sont conservés mais que certains trous sont rebouchés.

\begin{figure}[H]
	\centering
	\fboxsep=1.2pt
	\begin{subfigure}{.23\textwidth}
		\centering
		\includegraphics[scale=0.18]{../tps/TP1/P3_1_1_dilate_1}
		\caption{Dilatation n°1}
	\end{subfigure}
	\begin{subfigure}{.23\textwidth}
		\centering
		\includegraphics[scale=0.18]{../tps/TP1/P3_1_1_dilate_2}
		\caption{Dilatation n°2}
	\end{subfigure}
	\begin{subfigure}{.23\textwidth}
		\centering
		\includegraphics[scale=0.18]{../tps/TP1/P3_1_1_dilate_3}
		\caption{Dilatation n°3}
	\end{subfigure}
	\begin{subfigure}{.23\textwidth}
		\centering
		\includegraphics[scale=0.18]{../tps/TP1/P3_1_1_dilate_4}
		\caption{Dilatation n°4}
	\end{subfigure}

	\begin{subfigure}{.23\textwidth}
		\centering
		\includegraphics[scale=0.18]{../tps/TP1/P3_1_1_erode_1}
		\caption{Érosion n°1}
	\end{subfigure}
	\begin{subfigure}{.23\textwidth}
		\centering
		\includegraphics[scale=0.18]{../tps/TP1/P3_1_1_erode_2}
		\caption{Érosion n°2}
	\end{subfigure}
	\begin{subfigure}{.23\textwidth}
		\centering
		\includegraphics[scale=0.18]{../tps/TP1/P3_1_1_erode_3}
		\caption{Érosion n°3}
	\end{subfigure}
	\begin{subfigure}{.23\textwidth}
		\centering
		\includegraphics[scale=0.18]{../tps/TP1/P3_1_1_erode_4}
		\caption{Érosion n°4}
	\end{subfigure}
	\caption{Profils de ligne - Sobel}
	\label{fig:Erosions_Dilatations}
\end{figure}

\clearpage

Afin de réaliser ces opérations en une seule fois, il est possible d'utiliser le menu \textit{Image > Binary Morphology} pour indiquer le nombre d'itérations que l'on souhaite faire pour une opération donnée.

\begin{figure}[H]
	\centering
	\fboxsep=1.2pt
	\begin{subfigure}{.4\textwidth}
		\centering
		\includegraphics[scale=0.2]{../tps/TP1/P3_1_1_dilate_uneSeuleFois}
		\caption{Dilatation}
	\end{subfigure}
	\begin{subfigure}{.4\textwidth}
		\centering
		\includegraphics[scale=0.2]{../tps/TP1/P3_1_1_erode_uneSeuleFois}
		\caption{Érosion}
	\end{subfigure}
	\caption{Opérations effectuées en une seule fois}
	\label{fig:Dilatation_UneSeuleFois}
\end{figure}

Enfin, il est possible de modifier la taille de l'élément structurant pour les opérations de dilatation et d'érosion. Le résultat est quasiment identique à la répétition des opérations précédentes. La figure \ref{fig:Element_Structurant9x9} illustre ces traitements avec un élément structurant de taille 9x9. Il est donc possible d'utiliser ces deux traitements pour aboutir à des résultats très proches voire identiques.

\begin{figure}[H]
	\centering
	\fboxsep=1.2pt
	\begin{subfigure}{.4\textwidth}
		\centering
		\includegraphics[scale=0.2]{../tps/TP1/P3_1_1_dilate_element_structurant_9x9}
		\caption{Dilatation}
	\end{subfigure}
	\begin{subfigure}{.4\textwidth}
		\centering
		\includegraphics[scale=0.2]{../tps/TP1/P3_1_1_erode_element_structurant_9x9}
		\caption{Érosion}
	\end{subfigure}
	\caption{Opérations effectuées avec un élément structurant 9x9}
	\label{fig:Element_Structurant9x9}
\end{figure}

\textbf{2.} On souhaite supprimer les parasites et les trous des objets. Pour cela, il est nécessaire d'appliquer deux opérations appelées \textbf{Open} et \textbf{Close}. L'opération \textbf{Close} permet de progressivement combler l'intérieur des objets. Le nombre d'itérations doit être assez important si l'on souhaite les combler totalement. En revanche, l'opération \textbf{Open} a un comportement inverse. 

\textbf{3.} L'opération \textbf{Outline} permet de garder uniquement les contours des objets présents dans l'image.

\begin{figure}[H]
	\centering
	\fboxsep=1.2pt
	\includegraphics[scale=0.3]{../tps/TP1/P3_1_3}
	\caption{Opération de détection de contours}
	\label{fig:Outline}
\end{figure}

\section{Morphologie mathématique en niveaux de gris}

\textbf{1.} On souhaite réaliser les mêmes opérations que précédemment mais en omettant volontairement l'opération de binarisation pour rendre l'image en noir et blanc. En d'autres termes, on applique directement les traitements de dilatation et d'érosion sur l'image en niveaux de gris. 

\begin{figure}[H]
	\centering
	\begin{tabular}{rcc}
		& \textbf{Élément structurant 3x3} & \textbf{Élément structurant 7x7} \\
		\rotatebox{90}{\textbf{Dilatation}} & \includegraphics[scale=0.27]{../tps/TP1/P3_2_1_dilate_3} & \includegraphics[scale=0.27]{../tps/TP1/P3_2_1_dilate_7}\\
		\rotatebox{90}{\textbf{Érosion}} & \includegraphics[scale=0.27]{../tps/TP1/P3_2_1_erode_3} & \includegraphics[scale=0.27]{../tps/TP1/P3_2_1_erode_7}
	\end{tabular}
	\fboxsep=1.2pt
	\caption{Opérateurs de dilatation et d'érosion sur l'image en niveaux de gris}
	\label{fig:Ero_Dilat_NivGris}
\end{figure}

On peut remarquer que l'application de l'opération de dilatation fait apparaître un flou autour des objets. Quant à elle, l'opération d'érosion ternit les objets au niveau de leurs bordures. 

\bigskip

\textbf{2.} 

\begin{figure}[H]
	\centering
	\fboxsep=1.2pt
	\begin{subfigure}{.49\textwidth}
		\centering
		\includegraphics[scale=0.28]{../tps/TP1/P3_2_2_outline}
		\caption{Opération \textit{Outline}}
	\end{subfigure}
	\begin{subfigure}{.49\textwidth}
		\centering
		\includegraphics[scale=0.28]{../tps/TP1/P3_2_2_sobel}
		\caption{Opération \textit{Sobel}}
	\end{subfigure}
	\caption{Détection de contours par morphologie mathématique}
	\label{fig:Contours_Outline_Sobel}
\end{figure}

On peut remarquer que l'image obtenue après l'utilisation de \textbf{Outline} est nette et précise et ne fait ressortir que les contours des objets de l'image. \textbf{Sobel} permet de faire la même chose. Cependant, certains parasites sont toujours présents : les contours ne sont pas très nets. 

\clearpage

\listoffigures

\end{document}
