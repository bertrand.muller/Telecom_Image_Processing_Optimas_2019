\documentclass{tnreport3}
%\documentclass[stage2a]{tnreport1} % If you are in 2nd year
%\documentclass[confidential]{tnreport1} % If you are writing confidential report

\def\reportTitle{\textbf{Partie 5} - Reconnaissance de formes} % Titre du mémoire
\def\reportAuthor{Ophélien Amsler \& Bertrand Müller}

\usepackage{lipsum}
\usepackage{subcaption}
\usepackage{adjustbox}
\usepackage{dirtree}
\usepackage{tikz}
\usepackage{multirow}
\usetikzlibrary{trees}
\usepackage{tikz-qtree}
\usepackage{makecell}
\usepackage{hhline}
\usepackage{colortbl}
\usepackage[most]{tcolorbox}
\usepackage{pdfpages}
\usepackage{draftwatermark}
\usepackage[stable]{footmisc}
\usepackage{hhline}
\usepackage{float}
\usepackage{graphicx}
\usepackage{enumitem}
\usepackage{changepage}

\begin{document}

\maketitle

\clearpage

\renewcommand{\baselinestretch}{0.5}\normalsize
\tableofcontents
\renewcommand{\baselinestretch}{1.0}\normalsize

\clearpage

\chapter{Reconnaissance de formes}

\section{Reconnaissance d'objets - Classification}

\textbf{1.} Comme dans un précédent TP, on va chercher à binariser l'image source afin de séparer les objets du fond. Afin de choisir un bon seuil de binarisation, on a utilisé l'opération d'auto-seuillage de type \textbf{Exponential Fit} en vue de conserver les détails sur chacun des objets. On a également fait le choix d'appliquer une fermeture à l'image afin de combler les parties de l'image propres à chacun des objets. Le résultat de cette préparation est visible dans la figure \ref{fig:Choix_Seuil}.

\begin{figure}[H]
	\centering
	\fboxsep=1.2pt
	\includegraphics[scale=0.28]{../tps/TP3/P5_1_1_binarization}
	\caption{Binarisation de l'image}
	\label{fig:Choix_Seuil}
\end{figure}

\textbf{2.} Ensuite, on cherche à segmenter les objets présents dans l'image. Dans cette optique, il faut choisir les unités de représentation des données. Pour respecter les contraintes exposées, nous avons appliqué une segmentation de 1cm par pixel. Par la suite, nous avons défini les caractéristiques des objets pour nous permettre de compter chacun d'eux. Cette étape consiste donc à préciser si l'on souhaite préserver les trous, les objets imbriqués, à définir une longueur minimale de frontière et à choisir l'élément structurant pour le voisinage. Ici, il s'agit d'une frontière en 8-voisinage avec une longueur égale à 5 pixels. La figure \ref{fig:DataSampling_Area} montre la configuration adoptée.

\begin{figure}[H]
	\centering
	\fboxsep=1.2pt
	\includegraphics[scale=0.29]{../tps/TP3/P5_1_2_area}
	\caption{Définition des caractéristiques des objets}
	\label{fig:DataSampling_Area}
\end{figure}

On poursuit la recherche des objets dans une image en utilisant la fonction \textit{Data > Particules Count} du logiciel Optimas. Cela nous permet d'isoler les objets et surtout de les dénombrer. On compte bien \textbf{16} objets pour l'image \textbf{Objet1.tif} (12 objets et 4 trous) en laissant les paramètres par défaut. 

\begin{figure}[H]
	\centering
	\fboxsep=1.2pt
	\includegraphics[scale=0.35]{../tps/TP3/P5_1_2_count_Objets}
	\caption{Dénombrement des objets dans l'image \textbf{Objet1.tif}}
	\label{fig:Comptage_Objet1}
\end{figure}

Par ailleurs, les menus \textbf{Fill Hole} et \textbf{Border Kill} permettent de proposer d'autres moyens de compter les objets. Ils agissent comme des filtres. Par exemple, \textbf{Fill Hole} permet d'ignorer les trous au sein des objets. \textbf{Border Kill} ne compte pas les objets touchant les bords de l'image ou les bords d'une région d'intérêt. Dans les figures suivantes, on peut remarquer que le nombre d'objets détectés diffère selon les options choisies.

\bigskip

\begin{figure}[H]
	\centering
	\fboxsep=1.2pt
	\begin{subfigure}{.49\textwidth}
		\centering
		\includegraphics[scale=0.18]{../tps/TP3/P5_1_2_O1_count_Objets_nofill_noborderkill}
		\caption{Aucun filtre}
	\end{subfigure}
	\bigskip
	\begin{subfigure}{.49\textwidth}
		\centering
		\includegraphics[scale=0.18]{../tps/TP3/P5_1_2_O1_count_Objets_fill_noborderkill}
		\caption{\textbf{Fill Hole}}
	\end{subfigure}
	\bigskip
	\begin{subfigure}{1\textwidth}
	\centering
	\includegraphics[scale=0.18]{../tps/TP3/P5_1_2_O1_count_Objets_fill_leftRightBorders}
	\caption{\textbf{Border Kill}}
	\end{subfigure}
	\caption{Illustration des options \textbf{Fill Hole} et \textbf{Border Kill}}
	\label{fig:Fill_Hole_Border_Kill}
\end{figure}

\textbf{3.} Afin de différencier les objets sans préjuger de leur niveau de gris ou de leur position dans l'image, nous avons décidé d'extraire plusieurs caractéristiques pour les identifier. Ces derniers sont présentés dans la figure \ref{fig:Caracteristiques} avec l'image \textbf{Objet3.tif}. 

\begin{itemize}
	\item \textbf{Nombre d'Euler} : Il correspond à l'opération \textit{\textbf{1 - Nombre d'évidements de l'objet}}. De cette manière, une rondelle aura un nombre d'Euler égal à 0 puisqu'elle contient un trou. Voir la figure \ref{fig:Caracteristiques_Euler}.
	\bigskip
	\item \textbf{Circularité} : Cette caractéristique donne des informations sur la forme circulaire d'un objet. Plus la valeur est faible, plus l'objet est circulaire. Ceci peut être utile pour identifier les rondelles. Voir la figure \ref{fig:Caracteristiques_Circularity}.
	\bigskip
	\item \textbf{Nombre d'enfants} : Il s'agit d'une donnée donnant le nombre de sous-objets d'un objet. Par exemple, le dé avec la face 3 aura 3 enfants (correspondant aux 3 trous). Voir la figure \ref{fig:Caracteristiques_Children}.
	\bigskip
	\item \textbf{Nombre de pixels} : Il correspond au nombre de pixels utilisés pour représenter un objet. Selon des ordres de grandeur, il est possible de dire qu'une clé aura sûrement plus de pixels qu'un dé. Voir la figure \ref{fig:Caracteristiques_Pixels}.
	\bigskip
	\item \textbf{Rectangularité} : Cette caractéristique donne des informations sur la forme rectangulaire d'un objet. Plus la valeur est faible, plus l'objet est rectangulaire. Ceci peut être utile pour identifier les dés. Voir la figure \ref{fig:Caracteristiques_Rectangularity}.
\end{itemize}

\begin{figure}[H]
	\centering
	\fboxsep=1.2pt
	\begin{subfigure}{.49\textwidth}
		\centering
		\includegraphics[scale=0.19]{../tps/TP3/P5_1_3_O3_euler_number}
		\caption{Nombre d'Euler}
		\label{fig:Caracteristiques_Euler}
	\end{subfigure}
	\bigskip
	\begin{subfigure}{.49\textwidth}
		\centering
		\includegraphics[scale=0.19]{../tps/TP3/P5_1_3_O3_circularity}
		\caption{Circularité}
		\label{fig:Caracteristiques_Circularity}
	\end{subfigure}
	\bigskip
	\begin{subfigure}{.49\textwidth}
		\centering
		\includegraphics[scale=0.19]{../tps/TP3/P5_1_3_O3_number_of_children}
		\caption{Nombre d'enfants}
		\label{fig:Caracteristiques_Children}
	\end{subfigure}
	\begin{subfigure}{.49\textwidth}
		\centering
		\includegraphics[scale=0.19]{../tps/TP3/P5_1_3_O3_number_pixels}
		\caption{Nombre de pixels}
		\label{fig:Caracteristiques_Pixels}
	\end{subfigure}
	\begin{subfigure}{1\textwidth}
		\centering
		\includegraphics[scale=0.19]{../tps/TP3/P5_1_3_O3_rectangularity}
		\caption{Rectangularité}
		\label{fig:Caracteristiques_Rectangularity}
	\end{subfigure}
	\caption{Caractéristiques utiles pour l'identification d'objets}
	\label{fig:Caracteristiques}
\end{figure}

\textbf{4.} Suite à la présentation des caractéristiques à extraire, on souhaite les utiliser pour définir des classes d'objets et donc permettre une reconnaissance automatique des objets pour n'importe quelle image. La figure \ref{fig:Objects_Classes} montre les classes définies pour reconnaître des dés, des clés, des rondelles et des pièces. Elles ont été enregistrées dans des fichiers \textbf{.mac} pour pouvoir être importées à n'importe quel moment dans le logiciel. Pour information, la classe \textbf{Rebut} a été créée pour marquer les objets non reconnus par les autres classes.

\definecolor{mGreen}{rgb}{0,0.6,0}
\definecolor{mGray}{rgb}{0.5,0.5,0.5}
\definecolor{mPurple}{rgb}{0.58,0,0.82}
\definecolor{backgroundColour}{rgb}{0.95,0.95,0.92}

\lstdefinestyle{CStyle}{
	backgroundcolor=\color{backgroundColour},   
	commentstyle=\color{mGreen},
	keywordstyle=\color{magenta},
	numberstyle=\tiny\color{mGray},
	stringstyle=\color{mPurple},
	basicstyle=\footnotesize,
	breakatwhitespace=false,         
	breaklines=true,                 
	captionpos=b,                    
	keepspaces=true,                 
	numbers=left,                    
	numbersep=5pt,                  
	showspaces=false,                
	showstringspaces=false,
	showtabs=false,                  
	tabsize=2,
	language=C
}

\begin{figure}[H]
	\centering
	\fboxsep=1.2pt
	\begin{subfigure}{.47\textwidth}
		\centering
		\begin{lstlisting}[basicstyle=\scriptsize]
		Ar_De_Member_B =
					ArNestNofParents == 0
			&& 	ArEulerNumber < 1
			&& 	ArCircularity > 12
			&& 	ArRectangularity < 0.7
			&& 	ArPixelCounts > 500
			&& 	ArPixelCounts < 5000 ;
		\end{lstlisting}
		\caption{Classe "\textbf{Dé}"}
	\end{subfigure}
	\bigskip
	\begin{subfigure}{.49\textwidth}
		\centering
		\begin{lstlisting}[basicstyle=\scriptsize]
		 
		 
		 Ar_De1_Member_G = 
		 			ArNestNofChildren == 1
		 	&& 	Ar_De_Member_B ;
		 	
		 	
		\end{lstlisting}
		\caption{Classe "\textbf{Dé 1}"}
	\end{subfigure}
	\bigskip
	\begin{subfigure}{.49\textwidth}
		\centering
		\begin{lstlisting}[basicstyle=\scriptsize]
		Ar_De2_Member_C =
					ArNestNofChildren == 2
			&& 	Ar_De_Member_B ;
		\end{lstlisting}
		\caption{Classe "\textbf{Dé 2}"}
	\end{subfigure}
	\begin{subfigure}{.49\textwidth}
		\centering
		\begin{lstlisting}[basicstyle=\scriptsize]
		Ar_De3_Member_D =
					ArNestNofChildren == 3
			&& 	Ar_De_Member_B ;
		\end{lstlisting}
		\caption{Classe "\textbf{Dé 3}"}
	\end{subfigure}
	\begin{subfigure}{.49\textwidth}
		\centering
		\begin{lstlisting}[basicstyle=\scriptsize]
		Ar_De4_Member_E =
					ArNestNofChildren == 4
			&& Ar_De_Member_B ;
		\end{lstlisting}
		\caption{Classe "\textbf{Dé 4}"}
	\end{subfigure}
	\begin{subfigure}{.49\textwidth}
		\centering
		\begin{lstlisting}[basicstyle=\scriptsize]
		Ar_De5_Member_F =
					ArNestNofChildren == 5
			&& Ar_De_Member_B ;
		\end{lstlisting}
		\caption{Classe "\textbf{Dé 5}"}
	\end{subfigure}
	\begin{subfigure}{.49\textwidth}
		\centering
		\begin{lstlisting}[basicstyle=\scriptsize]
		Ar_De6_Member_H =
					ArNestNofChildren == 6
			&& Ar_De_Member_B ;
			
		\end{lstlisting}
		\caption{Classe "\textbf{Dé 6}"}
	\end{subfigure}
	\begin{subfigure}{.49\textwidth}
		\centering
		\begin{lstlisting}[basicstyle=\scriptsize]
		Ar_Rondelle_Member_J =
					ArNestNofParents == 0
			&& 	ArEulerNumber == 0
			&& 	ArCircularity < 15;
		\end{lstlisting}
		\caption{Classe "\textbf{Rondelle}"}
	\end{subfigure}
	\begin{subfigure}{.49\textwidth}
		\centering
		\begin{lstlisting}[basicstyle=\scriptsize]
		Ar_Cle_Member_A =
					ArNestNofParents == 0
			&& ArEulerNumber  == 1
			&& ArCircularity > 15
			&& ArPixelCounts > 1000
			&& ArMajorAxisLength > 9 ;
		\end{lstlisting}
		\caption{Classe "\textbf{Clé}"}
	\end{subfigure}
	\begin{subfigure}{.49\textwidth}
		\centering
		\begin{lstlisting}[basicstyle=\scriptsize]
		
		Ar_Piece_Member_I =
					ArEulerNumber == 1
			&& 	ArNestNofParents == 0
			&& 	ArCircularity < 15 ;
			
		\end{lstlisting}
		\caption{Classe "\textbf{Pièce}"}
	\end{subfigure}
	\begin{subfigure}{.49\textwidth}
		\centering
		\begin{lstlisting}[basicstyle=\scriptsize]	
		Ar_Rebut_Member_K = 
					! Ar_De_Member_B
			&& 	! Ar_Cle_Member_A
			&& 	! Ar_Piece_Member_I
			&& 	! Ar_Rondelle_Member_J 
			&& 	ArNestNofParents == 0 ;
		\end{lstlisting}
		\caption{Classe "\textbf{Rebut}"}
	\end{subfigure}
	\caption{Caractéristiques utiles pour l'identification d'objets}
	\label{fig:Objects_Classes}
\end{figure}

Grâce à notre classification, il est désormais possible de reconnaître les objets souhaités parmi les images qui sont à notre disposition. Les figures suivantes ont été obtenues à partir de cette classification et du menu d'extraction des données du logiciel Optimas. 

\bigskip

\begin{figure}[H]
	\begin{adjustwidth}{-3cm}{-3cm}
		\centering
		\fboxsep=1.2pt
		\begin{subfigure}{.29\textwidth}
			\centering
			\includegraphics[scale=0.28]{../tps/TP3/P5_1_4_Cles_recognition}
			\caption{\textbf{Cles.tif}}
		\end{subfigure}
		\hspace{11mm}
		\begin{subfigure}{.29\textwidth}
			\centering
			\includegraphics[scale=0.28]{../tps/TP3/P5_1_4_Des_recognition}
			\caption{\textbf{Des.tif}}
		\end{subfigure}
		\hspace{8mm}
		\begin{subfigure}{.29\textwidth}
			\centering
			\includegraphics[scale=0.28]{../tps/TP3/P5_1_4_Pieces_recognition}
			\caption{\textbf{Pieces.tif}}
		\end{subfigure}
		\linebreak
		\begin{subfigure}{.29\textwidth}
			\centering
			\includegraphics[scale=0.28]{../tps/TP3/P5_1_4_Rondelle_recognition}
			\caption{\textbf{Rondel.tif}}
		\end{subfigure}
		\hspace{8mm}
		\begin{subfigure}{.29\textwidth}
			\centering
			\includegraphics[scale=0.28]{../tps/TP3/P5_1_4_O1_recognition}
			\caption{\textbf{Objet1.tif}}
		\end{subfigure}
		\hspace{11mm}
		\begin{subfigure}{.29\textwidth}
			\centering
			\includegraphics[scale=0.28]{../tps/TP3/P5_1_4_O2_recognition}
			\caption{\textbf{Objet2.tif}}
		\end{subfigure}
		\linebreak
		\begin{subfigure}{.29\textwidth}
			\centering
			\includegraphics[scale=0.28]{../tps/TP3/P5_1_4_O3_recognition}
			\caption{\textbf{Objet3.tif}}
		\end{subfigure}
		\hspace{11mm}
		\begin{subfigure}{.29\textwidth}
			\centering
			\includegraphics[scale=0.28]{../tps/TP3/P5_1_4_O4_recognition}
			\caption{\textbf{Objet4.tif}}
		\end{subfigure}
		\hspace{8mm}
		\begin{subfigure}{.29\textwidth}
			\centering
			\includegraphics[scale=0.28]{../tps/TP3/P5_1_4_O5_recognition}
			\caption{\textbf{Objet5.tif}}
		\end{subfigure}
		\linebreak
		\begin{subfigure}{.29\textwidth}
			\centering
			\includegraphics[scale=0.28]{../tps/TP3/P5_1_4_O6_recognition}
			\caption{\textbf{Objet6.tif}}
		\end{subfigure}
		\hspace{11mm}
		\begin{subfigure}{.29\textwidth}
			\centering
			\includegraphics[scale=0.28]{../tps/TP3/P5_1_4_O7_recognition}
			\caption{\textbf{Objet7.tif}}
			\label{fig:Objet7}
		\end{subfigure}
		\hspace{8mm}
		\begin{subfigure}{.29\textwidth}
			\centering
			\includegraphics[scale=0.28]{../tps/TP3/P5_1_4_O2D_recognition}
			\caption{\textbf{Objet2\_D.tif}}
		\end{subfigure}
		\caption{Résultats de la reconnaissance d'objets par classification}
		\label{fig:Images_Recognition}
	\end{adjustwidth}
\end{figure}

\textbf{5.} La dernière étape de ce TP consiste à créer un fichier \textbf{.mac} pour automatiser les traitements précédents. Ainsi, on aura juste à choisir une image et la reconnaissance des objets se lancera automatiquement sur celle-ci. La figure \ref{fig:Macro_Recognition} montre le programme utilisé pour la reconnaissance automatique de formes. 

\lstdefinestyle{base}{
	language=C,
	emptylines=1,
	breaklines=true,
	basicstyle=\ttfamily\color{black},
	moredelim=**[is][\color{red}]{@}{@},
}


\begin{figure}[H]
	\begin{lstlisting}[style=base,basicstyle=\scriptsize]
	// Chargement des macros utiles au programme
	RunMacro ("C:/PROGRAM FILES (X86)/OPTIMAS 6.5/macsrc/athld/athld.mac");
	RunMacro ("C:/PROGRAM FILES (X86)/OPTIMAS 6.5/macsrc/binary/binary.mac");
	
	// Binarisation de l'image
	GrayToBinary ();
	Threshold ( 127.5:255.0 );
	BINB_iIterations = 1;
	DilateFilter(,BINB_iIterations);
	ErodeFilter(,BINB_iIterations);
	Threshold ( 127.5:255.0 );
	CloseWindow ( "Binary Morphology" );
	Delete(ATHLD_szPhaseList); //Closes Auto Threshold dialog
	
	// Extraction des objets de l'image
	ActivateMeasurementSet("Area Morphometry Set");
	RunMacro ("C:/PROGRAM FILES (X86)/OPTIMAS 6.5/MACSRC/PC/pps.mac");
	PPS_CountMacro ();
	DataCollection (0);
	MultipleExtract (TRUE);
	CloseWindow ("Measurement Explorer");
	CloseWindow("Particle Count");
	\end{lstlisting}
	\caption{Macro de reconnaissance automatique d'objets}
	\label{fig:Macro_Recognition}
\end{figure}

Comme montré dans la figure \ref{fig:Objet7}, un problème se présente puisque des objets ne sont pas visibles entièrement (ils touchent des bords de l'image). Pour cela, il aurait fallu appliquer un traitement supplémentaire en vue de retirer tous ces objets. Comme ils n'ont pas été retirés avant extraction, certains d'entre eux sont considérés comme des rebuts. Or, ce n'est pas le cas dans la réalité. Il aurait donc été judicieux d'utiliser la fonction \textbf{\textit{Measurements > Border Kill}} pour les supprimer avant de les comptabiliser. En dehors de tout traitement numérique de l'image, on aurait également pu veiller à bien placer les objets lors de l'acquisition. 

\clearpage

\listoffigures

\end{document}
